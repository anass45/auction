# Auction

## It is an implementation of the auction problem.
The goal is to implement an algorithm for finding the winner AND the winning price

## Running Main example
The main class is Auction

This project uses Java 17

## Running Tests
To execute the tests either run `mvn test` or run the tests from the IDE you are using
