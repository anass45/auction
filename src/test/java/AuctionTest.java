import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * This class allow to test Auction problem
 */
public class AuctionTest {

    private static final Integer RESERVE_PRICE = 100;

    @Test
    void people_should_place_bids() {
        Map<String, List<Integer>> bids = Map.ofEntries(
                Map.entry("A", List.of(110, 130)),
                Map.entry("B", List.of()),
                Map.entry("C", List.of(125)),
                Map.entry("D", List.of(105, 115, 90)));

        var optionalWinner = Auction.getWinner(bids, RESERVE_PRICE);
        assertTrue(optionalWinner.isPresent());
    }

    @Test
    void winner_should_have_the_highest_bid() {
        Map<String, List<Integer>> bids = Map.ofEntries(
                Map.entry("A", List.of(110, 130)),
                Map.entry("D", List.of(105, 115, 90)));

        var winner = Auction.getWinner(bids, RESERVE_PRICE).get();
        assertEquals("A", winner.winnerName());
    }

    @Test
    void should_not_have_winner_when_no_single_maximal_bids() {
        Map<String, List<Integer>> bids = Map.ofEntries(
                Map.entry("A", List.of(110, 130)),
                Map.entry("B", List.of(105, 130)));

        var optionalWinner = Auction.getWinner(bids, RESERVE_PRICE);
        assertTrue(optionalWinner.isEmpty());
    }

    @Test
    void should_not_have_winner_when_no_bids_above_reserve_price() {
        Map<String, List<Integer>> bids = Map.ofEntries(
                Map.entry("A", List.of(80)),
                Map.entry("B", List.of(90)));

        var optionalWinner = Auction.getWinner(bids, RESERVE_PRICE);
        assertTrue(optionalWinner.isEmpty());
    }

    @Test
    void winner_price_should_be_the_highest_bid_from_non_winning_buyer() {
        Map<String, List<Integer>> bids = Map.ofEntries(
                Map.entry("A", List.of(110, 130)),
                Map.entry("D", List.of(105, 115, 90)));

        var winner = Auction.getWinner(bids, RESERVE_PRICE).get();
        assertEquals(115, winner.winningPrice());
    }

    @Test
    void winner_price_should_be_the_reserve_price() {
        Map<String, List<Integer>> bids = Map.ofEntries(
                Map.entry("A", List.of(110, 130)),
                Map.entry("D", List.of(80,90)));

        var winner = Auction.getWinner(bids, RESERVE_PRICE).get();
        assertEquals(100, winner.winningPrice());
    }
}
