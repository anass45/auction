import java.util.*;

public class Auction {

    record Winner(String winnerName, int winningPrice) {
    }

    public static void main(String[] args) {
        Map<String, List<Integer>> bids = Map.ofEntries(
                Map.entry("A", List.of(110, 130)),
                Map.entry("B", List.of()),
                Map.entry("C", List.of(125)),
                Map.entry("D", List.of(105, 115, 90)),
                Map.entry("E", List.of(132, 135, 140))
        );
        var reservePrice = 100;
        var optionalWinner = getWinner(bids, reservePrice);
        if (optionalWinner.isEmpty()) {
            System.out.println("No Winner");
        } else {
            var winner = optionalWinner.get();
            System.out.printf("%s wins the auction at the price of %s€%n", winner.winnerName, winner.winningPrice);
        }
    }

    /**
     * Determines the potential winner and the price they will have to pay
     *
     * @param peopleBids represents the list of people participating in the auction and their respective bids
     * @param reservePrice represents the auction reserve price
     */
    public static Optional<Winner> getWinner(Map<String, List<Integer>> peopleBids, int reservePrice) {
        var peopleMaxAdmissibleBid = getMaxAdmissibleBidForEachPeople(peopleBids, reservePrice);
        if (peopleMaxAdmissibleBid.isEmpty()) return Optional.empty();
        Winner winner;
        var sortedBids = peopleMaxAdmissibleBid.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).toList();
        var firstBid = sortedBids.get(0);

        if (sortedBids.size() == 1) { // If only one admissible bid, the winner will pay the reserve price
            winner = new Winner(firstBid.getKey(), reservePrice);
        } else { // The winning price is the highest bid price from a non-winning buyer above the reserve price
            var secondBid = sortedBids.get(1);
            if(secondBid.getValue().equals(firstBid.getValue())){ // No winner because there is no single maximal bid
                return Optional.empty();
            }
            else {
                winner = new Winner(firstBid.getKey(), secondBid.getValue());
            }
        }
        return Optional.of(winner);
    }

    /**
     * Allows to recover the maximum bid above the reserve price for each user
     *
     * @param peopleBids represents the list of people participating in the auction and their respective bids
     * @param reservePrice represents the auction reserve price
     */
    private static HashMap<String, Integer> getMaxAdmissibleBidForEachPeople(Map<String, List<Integer>> peopleBids, int reservePrice) {
        var peopleMaxAdmissibleBid = new HashMap<String, Integer>();
        peopleBids.forEach((people, bids) -> {
            var maxAdmissibleBid = findMaxBidGreaterOrEqualThanReservePrice(bids, reservePrice);
            maxAdmissibleBid.ifPresent(bid -> peopleMaxAdmissibleBid.put(people, bid));
        });
        return peopleMaxAdmissibleBid;
    }

    /**
     * Allows to recover the maximum value above the reserve price
     *
     * @param bids list of auctions of a people
     * @param reservePrice represents the auction reserve price
     */
    private static Optional<Integer> findMaxBidGreaterOrEqualThanReservePrice(List<Integer> bids, int reservePrice) {
        return bids.stream()
                .filter(Objects::nonNull)
                .filter(bid -> bid >= reservePrice)
                .max(Comparator.naturalOrder());
    }

}
